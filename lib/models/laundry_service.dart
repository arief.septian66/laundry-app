part of 'models.dart';

class LaundryService {
  late String idService;
  late String nameService;
  late int count;
  late int finishDuration;
  late int idPcs;
  late int price;
  late int delete;
  late String pcsName;
  late String createdAt;
  late String updatedAt;

  LaundryService(
      {required this.idService,
      required this.nameService,
      required this.count,
      required this.finishDuration,
      required this.idPcs,
      required this.price,
      required this.delete,
      required this.pcsName,
      required this.createdAt,
      required this.updatedAt});

  LaundryService.fromJson(Map<String, dynamic> jsonMap) {
    this.idService = jsonMap['idlayanan'];
    this.nameService = jsonMap['nama_layanan'];
    this.count = jsonMap['jumlah'];
    this.finishDuration = jsonMap['durasi_penyelesaian'];
    this.idPcs = jsonMap['idsatuan'];
    this.price = jsonMap['harga'].toInt();
    this.delete = jsonMap['hapus'];
    this.pcsName = jsonMap['nama_satuan'];
    this.createdAt = jsonMap['created_at'];
    this.updatedAt = jsonMap['updated_at'];
  }
}

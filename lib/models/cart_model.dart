part of 'models.dart';

class Cart {
  int qty;
  LaundryService service;

  Cart({required this.qty, required this.service});
}

part of 'models.dart';

class TransactionModel {
  late String? id;
  late String name;
  late String transactionDate;
  late String finishDate;
  late String note;
  late String? totalBill;
  late List<Cart>? cartData;

  TransactionModel(
      {required this.name,
      required this.transactionDate,
      required this.finishDate,
      required this.note,
      this.totalBill,
      this.id,
      this.cartData});
}

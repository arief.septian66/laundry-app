part of 'models.dart';

class ApiReturnValue<T> {
  T? data;
  RequestStatus status;

  ApiReturnValue({required this.data, required this.status});
}

enum RequestStatus {
  success_request,
  failed_request,
  error_parsing,
  server_error,
  internet_issue
}

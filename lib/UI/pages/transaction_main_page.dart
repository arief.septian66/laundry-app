part of 'pages.dart';

class TransactionMainPage extends StatefulWidget {
  TransactionMainPage({Key? key}) : super(key: key);

  @override
  _TransactionMainPageState createState() => _TransactionMainPageState();
}

class _TransactionMainPageState extends State<TransactionMainPage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController noteController = TextEditingController();

  DateTime? transactionDate;
  DateTime? finishDate;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BasicAppBar.basicAppBar(
        title: 'Masukkan Transaksi',
      ),
      body: _buildContent(),
      backgroundColor: Colors.white,
    );
  }

  Widget _buildContent() {
    Widget _buildNameField() {
      return TextFieldCustom.roundedTextfield(
          label: 'Nama', controller: nameController);
    }

    Widget _buildButtonDatePicker() {
      return GestureDetector(
        onTap: () async {
          DatePicker.showDatePicker(context,
              showTitleActions: true,
              minTime: DateTime.now(),
              maxTime: DateTime.now().add(Duration(days: 7)),
              onConfirm: (date) {
            setState(() {
              transactionDate = date;
            });
          }, currentTime: DateTime.now(), locale: LocaleType.id);
        },
        child: Container(
            margin: EdgeInsets.only(top: 3.0.w),
            width: double.infinity,
            padding: EdgeInsets.symmetric(vertical: 2.0.h),
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(10)),
            alignment: Alignment.center,
            child: Text('PILIH TANGGAL TRANSAKSI',
                style: TextStyle(
                    fontSize: 11.0.sp,
                    color: Colors.white,
                    fontWeight: FontWeight.bold))),
      );
    }

    Widget _buildButtonServicePicker() {
      return GestureDetector(
        onTap: () async {
          bool? somethingChange = await Navigator.push(context,
              MaterialPageRoute(builder: (_) => LaundryServiceListPage()));

          if (somethingChange != null) {
            setState(() {});
          }
        },
        child: Container(
            margin: EdgeInsets.only(top: 3.0.w),
            width: double.infinity,
            padding: EdgeInsets.symmetric(vertical: 2.0.h),
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(10)),
            alignment: Alignment.center,
            child: Text('PILIH LAYANAN',
                style: TextStyle(
                    fontSize: 11.0.sp,
                    color: Colors.white,
                    fontWeight: FontWeight.bold))),
      );
    }

    Widget _buildCartWidget() {
      Widget _buildDataCart(List<Cart> data) {
        List<Widget> _dataCartWidget = [];

        for (var i = 0; i < data.length; i++) {
          _dataCartWidget.add(Container(
              margin: EdgeInsets.only(
                  top: 5.0.w, bottom: i == data.length - 1 ? 5.0.w : 0),
              child: Row(
                children: [
                  Flexible(
                    flex: 2,
                    child: Container(
                        width: double.infinity,
                        alignment: Alignment.centerLeft,
                        child: Icon(
                          Icons.local_laundry_service_rounded,
                          color: Theme.of(context).primaryColor,
                        )),
                  ),
                  Flexible(
                      flex: 8,
                      child: Container(
                          width: double.infinity,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                  width: double.infinity,
                                  child: Text(data[i].service.nameService,
                                      style: TextStyle(
                                          fontSize: 12.0.sp,
                                          color: Theme.of(context).primaryColor,
                                          fontWeight: FontWeight.bold))),
                              RichText(
                                  text: TextSpan(
                                      style: TextStyle(
                                          fontSize: 10.0.sp,
                                          color: Colors.black87),
                                      children: [
                                    TextSpan(
                                        text: data[i].qty.toString() +
                                            ' ${data[i].service.pcsName} '),
                                    TextSpan(
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                        text: FunctionHelper.moneyChanger(
                                            double.parse((data[i].qty *
                                                    data[i].service.price)
                                                .toString()))),
                                  ]))
                            ],
                          )))
                ],
              )));
        }
        return Column(
          children: _dataCartWidget,
        );
      }

      Widget _buildEstimationDate(List<Cart> data) {
        int longestWork = 0;

        Widget buildEstimateDateData(
            {required String title, required String desc}) {
          return Row(
            children: [
              Flexible(
                flex: 2,
                child: Container(
                    width: double.infinity,
                    alignment: Alignment.centerLeft,
                    child: Icon(
                      Icons.date_range,
                      color: Theme.of(context).primaryColor,
                    )),
              ),
              Flexible(
                  flex: 8,
                  child: Container(
                      width: double.infinity,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                              width: double.infinity,
                              child: Text(title,
                                  style: TextStyle(
                                      fontSize: 12.0.sp,
                                      color: Theme.of(context).primaryColor,
                                      fontWeight: FontWeight.bold))),
                          SizedBox(
                              width: double.infinity,
                              child: Text(desc,
                                  style: TextStyle(
                                    fontSize: 10.0.sp,
                                    color: Colors.black87,
                                  ))),
                        ],
                      )))
            ],
          );
        }

        for (var i = 0; i < data.length; i++) {
          if ((data[i].qty * data[i].service.finishDuration) > longestWork) {
            longestWork = data[i].qty * data[i].service.finishDuration;
          }
        }

        finishDate = transactionDate!.add(Duration(milliseconds: longestWork));

        return Container(
          padding: EdgeInsets.symmetric(vertical: 5.0.w),
          child: Column(
            children: [
              buildEstimateDateData(
                  title: 'Tanggal Transaksi',
                  desc: FunctionHelper.dateToReadable(transactionDate!)),
              SizedBox(height: 5.0.w),
              buildEstimateDateData(
                  title: 'Tanggal Selesai',
                  desc: FunctionHelper.dateToReadable(finishDate!))
            ],
          ),
        );
      }

      return BlocBuilder<CartCubit, CartState>(
        bloc: BlocProvider.of<CartCubit>(context),
        builder: (context, state) {
          if (state is CartValue) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    margin: EdgeInsets.only(top: 5.0.w),
                    width: 90.0.w,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: Colors.black.withOpacity(0.07)),
                    padding: EdgeInsets.symmetric(horizontal: 5.0.w),
                    child: _buildDataCart(state.data)),
                _buildButtonServicePicker(),
                transactionDate == null
                    ? Container()
                    : Container(
                        margin: EdgeInsets.only(top: 5.0.w),
                        width: 90.0.w,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                            color: Colors.black.withOpacity(0.07)),
                        padding: EdgeInsets.symmetric(horizontal: 5.0.w),
                        child: _buildEstimationDate(state.data)),
                _buildButtonDatePicker(),
                TextFieldCustom.roundedTextfield(
                    controller: noteController,
                    label: 'Keterangan (Opsional)',
                    noBorder: true)
              ],
            );
          } else {
            return _buildButtonServicePicker();
          }
        },
      );
    }

    return Stack(
      children: [
        SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 5.0.w),
            width: 100.0.w,
            height: 100.0.h,
            child: Column(
              children: [
                _buildNameField(),
                _buildCartWidget(),
              ],
            ),
          ),
        ),
        Positioned(
            bottom: 5.0.w,
            right: 5.0.w,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(primary: Color(0xFFf7a832)),
              onPressed: transactionDate != null
                  ? () {
                      TransactionModel trasaction = TransactionModel(
                          name: nameController.text,
                          transactionDate:
                              FunctionHelper.dateToReadable(transactionDate!),
                          note: noteController.text,
                          finishDate:
                              FunctionHelper.dateToReadable(finishDate!));
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => SummaryPage(
                                    transaction: trasaction,
                                  )));
                    }
                  : null,
              child: Text(
                'LANJUTKAN',
                style: TextStyle(
                    fontSize: 12.0.sp,
                    color:
                        transactionDate == null ? Colors.white : Colors.black87,
                    fontWeight: FontWeight.bold),
              ),
            ))
      ],
    );
  }
}

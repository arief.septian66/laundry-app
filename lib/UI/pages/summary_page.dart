part of 'pages.dart';

class SummaryPage extends StatefulWidget {
  final TransactionModel transaction;
  SummaryPage({Key? key, required this.transaction}) : super(key: key);

  @override
  _SummaryPageState createState() => _SummaryPageState();
}

class _SummaryPageState extends State<SummaryPage> {
  TransactionModel? data;
  double totalBill = 0;

  @override
  void initState() {
    super.initState();
    setState(() {
      data = widget.transaction;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: BasicAppBar.basicAppBar(title: 'Review Transaksi'),
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    Widget dataWidget({required String title, required String desc}) {
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 5.0.w),
        width: 90.0.w,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 5.0.w),
            Text(title,
                style: TextStyle(fontSize: 10.0.sp, color: Colors.black87)),
            Container(
                margin: EdgeInsets.only(bottom: 3.0.w, top: 1.0.w),
                child: Text(desc,
                    style: TextStyle(
                        fontSize: 12.0.sp,
                        color: Colors.black87,
                        fontWeight: FontWeight.bold))),
            Container(
              width: double.infinity,
              height: 0.5.w,
              color: Colors.black12,
            )
          ],
        ),
      );
    }

    Widget _buildDataCart(List<Cart> data) {
      List<Widget> _dataCartWidget = [];

      for (var i = 0; i < data.length; i++) {
        _dataCartWidget.add(Container(
            margin: EdgeInsets.only(
                top: 5.0.w, bottom: i == data.length - 1 ? 5.0.w : 0),
            child: Row(
              children: [
                Flexible(
                  flex: 2,
                  child: Container(
                      width: double.infinity,
                      alignment: Alignment.centerLeft,
                      child: Icon(
                        Icons.local_laundry_service_rounded,
                        color: Theme.of(context).primaryColor,
                      )),
                ),
                Flexible(
                    flex: 8,
                    child: Container(
                        width: double.infinity,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                                width: double.infinity,
                                child: Text(data[i].service.nameService,
                                    style: TextStyle(
                                        fontSize: 12.0.sp,
                                        color: Theme.of(context).primaryColor,
                                        fontWeight: FontWeight.bold))),
                            RichText(
                                text: TextSpan(
                                    style: TextStyle(
                                        fontSize: 10.0.sp,
                                        color: Colors.black87),
                                    children: [
                                  TextSpan(
                                      text: data[i].qty.toString() +
                                          ' ${data[i].service.pcsName} '),
                                  TextSpan(
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                      text: FunctionHelper.moneyChanger(
                                          double.parse((data[i].qty *
                                                  data[i].service.price)
                                              .toString()))),
                                ]))
                          ],
                        )))
              ],
            )));
      }
      return Column(
        children: _dataCartWidget,
      );
    }

    return Stack(
      children: [
        Container(
            width: 100.0.w,
            height: 100.0.h,
            child: BlocBuilder<CartCubit, CartState>(
              bloc: BlocProvider.of<CartCubit>(context),
              builder: (context, state) {
                if (state is CartValue) {
                  for (var i = 0; i < state.data.length; i++) {
                    totalBill = totalBill +
                        (state.data[i].qty * state.data[i].service.price);
                  }
                  return SingleChildScrollView(
                    child: Column(
                      children: [
                        dataWidget(title: 'Nama Pelanggan', desc: data!.name),
                        Container(
                            margin: EdgeInsets.only(top: 5.0.w),
                            width: 90.0.w,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                color: Colors.black.withOpacity(0.07)),
                            padding: EdgeInsets.symmetric(horizontal: 5.0.w),
                            child: _buildDataCart(state.data)),
                        dataWidget(
                            title: 'Tanggal Pembuatan',
                            desc: data!.transactionDate),
                        dataWidget(
                            title: 'Tanggal Selesai', desc: data!.finishDate),
                        dataWidget(title: 'Keterangan', desc: data!.note),
                        dataWidget(
                            title: 'Total Tagihan',
                            desc: FunctionHelper.moneyChanger(totalBill))
                      ],
                    ),
                  );
                }
                return Container();
              },
            )),
        Positioned(
            bottom: 5.0.w,
            right: 5.0.w,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(primary: Color(0xFFf7a832)),
              onPressed: () async {
                BlocProvider.of<CartCubit>(context).checkout(
                    context,
                    TransactionModel(
                        name: widget.transaction.name,
                        transactionDate: widget.transaction.transactionDate,
                        finishDate: widget.transaction.finishDate,
                        note: widget.transaction.note,
                        totalBill: totalBill.toInt().toString()));
              },
              child: Text(
                'SIMPAN TRANSAKSI',
                style: TextStyle(
                    fontSize: 12.0.sp,
                    color: Colors.black87,
                    fontWeight: FontWeight.bold),
              ),
            ))
      ],
    );
  }
}

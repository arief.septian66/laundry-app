part of 'pages.dart';

class LaundryServiceListPage extends StatefulWidget {
  LaundryServiceListPage({Key? key}) : super(key: key);

  @override
  _LaundryServiceListPageState createState() => _LaundryServiceListPageState();
}

class _LaundryServiceListPageState extends State<LaundryServiceListPage> {
  ServiceCubit serviceCubit = ServiceCubit();
  TextEditingController searchController = TextEditingController();
  Timer? _debounce;
  final PagingController<int, LaundryService> _pagingController =
      PagingController(firstPageKey: 0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: BasicAppBar.basicAppBar(title: 'Daftar Layanan'),
      body: _buildContent(),
    );
  }

  _onSearchChanged(String query) {
    if (_debounce?.isActive ?? false) _debounce!.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      serviceCubit.loadData(q: query, offset: 0);
    });
  }

  @override
  void dispose() {
    _debounce?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage();
    });
    super.initState();
    serviceCubit.loadData(offset: 0);
  }

  _fetchPage() {
    ServiceState serviceState = serviceCubit.state;
    setState(() {
      serviceCubit.loadData(
          offset: serviceState is ServiceLoaded ? serviceState.data.length : 0);
    });
  }

  Widget _buildContent() {
    Widget _buildSearchField() {
      return TextFieldCustom.roundedTextfield(
          onChanged: (value) {
            _onSearchChanged(value);
          },
          controller: searchController,
          label: 'Cari Layanan',
          noBorder: true);
    }

    Widget _buildContentData(List<LaundryService> data) {
      Widget buildDataService(int i) {
        return GestureDetector(
          onTap: () {
            TextEditingController controllerAdd = TextEditingController();

            showModalBottomSheet(
                context: context,
                builder: (context) {
                  return Stack(
                    children: [
                      Container(
                          height: 35.0.h,
                          color: Colors.white,
                          child: SingleChildScrollView(
                              child: Column(
                            children: [
                              SizedBox(height: 5.0.h),
                              Container(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 5.0.w),
                                  width: 100.0.w,
                                  child: TextFieldCustom.roundedTextfield(
                                      textInputType: TextInputType.number,
                                      controller: controllerAdd,
                                      label: 'Jumlah (${data[i].pcsName})')),
                              GestureDetector(
                                onTap: () {
                                  if (controllerAdd.text.length == 0) {
                                    Fluttertoast.showToast(
                                        msg: 'Isi Jumlahnya Terlebih Dahulu!');
                                  } else {
                                    BlocProvider.of<CartCubit>(context)
                                        .addToCart(data[i],
                                            int.parse(controllerAdd.text));
                                    Navigator.pop(context);
                                    Navigator.pop(context, true);
                                  }
                                },
                                child: Container(
                                    margin: EdgeInsets.all(
                                      5.0.w,
                                    ),
                                    width: 90.0.w,
                                    padding:
                                        EdgeInsets.symmetric(vertical: 1.5.h),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Color(0xFFf7a832)),
                                    alignment: Alignment.center,
                                    child: Text(
                                      'TAMBAH',
                                      style: TextStyle(
                                          fontSize: 12.0.sp,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black87),
                                    )),
                              ),
                            ],
                          ))),
                      Container(
                        color: Colors.white,
                        width: 100.0.w,
                        padding: EdgeInsets.symmetric(
                            vertical: 2.0.h, horizontal: 5.0.w),
                        child: Text('Tambah ' + data[i].nameService,
                            style: TextStyle(
                                fontSize: 13.0.sp,
                                color: Colors.black87,
                                fontWeight: FontWeight.bold)),
                      )
                    ],
                  );
                });
          },
          child: Container(
              margin: EdgeInsets.only(
                  top: 10.0.w, bottom: i == data.length - 1 ? 10.0.w : 0),
              child: Row(
                children: [
                  Flexible(
                    flex: 2,
                    child: Container(
                        width: double.infinity,
                        alignment: Alignment.centerLeft,
                        child: Icon(
                          Icons.local_laundry_service_rounded,
                          color: Theme.of(context).primaryColor,
                        )),
                  ),
                  Flexible(
                    flex: 8,
                    child: Container(
                        width: double.infinity,
                        child: Column(
                          children: [
                            SizedBox(
                                width: double.infinity,
                                child: Text(data[i].nameService,
                                    style: TextStyle(
                                        fontSize: 12.0.sp,
                                        color: Theme.of(context).primaryColor,
                                        fontWeight: FontWeight.bold))),
                            SizedBox(
                                width: double.infinity,
                                child: Text(
                                    'Harga ' +
                                        FunctionHelper.moneyChanger(
                                            double.parse(
                                                data[i].price.toString())),
                                    style: TextStyle(
                                        fontSize: 11.0.sp,
                                        color: Colors.black87))),
                            SizedBox(
                                width: double.infinity,
                                child: Text(
                                    'Pengerjaan ' +
                                        FunctionHelper.milisecondToDays(
                                            data[i].finishDuration) +
                                        ' / ' +
                                        data[i].pcsName,
                                    style: TextStyle(
                                        fontSize: 11.0.sp,
                                        color: Colors.black87))),
                          ],
                        )),
                  )
                ],
              )),
        );
      }

      return LazyLoadScrollView(
        onEndOfPage: () => _fetchPage(),
        child: ListView.builder(
          itemCount: data.length,
          itemBuilder: (context, position) {
            return buildDataService(position);
          },
        ),
      );
    }

    return Container(
      width: 100.0.w,
      padding: EdgeInsets.symmetric(horizontal: 5.0.w),
      child: Stack(
        children: [
          BlocBuilder<ServiceCubit, ServiceState>(
            bloc: serviceCubit,
            builder: (context, state) {
              if (state is ServiceLoading) {
                return Center(child: CircularProgressIndicator());
              } else if (state is ServiceLoaded) {
                return Container(
                    margin: EdgeInsets.only(
                      top: 14.0.w + 12.0.sp,
                    ),
                    child: _buildContentData(state.data));
              }
              return Container();
            },
          ),
          _buildSearchField(),
        ],
      ),
    );
  }
}

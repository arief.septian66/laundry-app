part of 'pages.dart';

class TransactionHistory extends StatefulWidget {
  TransactionHistory({Key? key}) : super(key: key);

  @override
  _TransactionHistoryState createState() => _TransactionHistoryState();
}

class _TransactionHistoryState extends State<TransactionHistory> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BasicAppBar.basicAppBar(title: 'Daftar Transaksi'),
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    Widget _buildData(QuerySnapshot<Object?> data) {
      Widget buildDataFinal(TransactionModel transaction,
          QuerySnapshot<Object?> detailTransaction) {
        List<Cart> dataTransaction = [];
        for (var i = 0; i < detailTransaction.docs.length; i++) {
          if (detailTransaction.docs[i]['transaction_id'] == transaction.id) {
            dataTransaction.add(Cart(
                qty: detailTransaction.docs[i]['qty'],
                service: LaundryService(
                    idService: detailTransaction.docs[i]['service_id'],
                    nameService: detailTransaction.docs[i]['service_name'],
                    price: detailTransaction.docs[i]['service_price'],
                    count: 0,
                    createdAt: '',
                    delete: 0,
                    finishDuration: 0,
                    idPcs: 0,
                    pcsName: '',
                    updatedAt: '')));
          }
        }

        List<Widget> dataListLaundry = [];

        for (var i = 0; i < dataTransaction.length; i++) {
          dataListLaundry.add(Row(
            children: [
              Flexible(
                flex: 2,
                child: Container(
                    width: double.infinity,
                    alignment: Alignment.centerLeft,
                    child: Icon(
                      Icons.local_laundry_service_rounded,
                      color: Theme.of(context).primaryColor,
                    )),
              ),
              Flexible(
                flex: 8,
                child: Container(
                    width: double.infinity,
                    child: Column(
                      children: [
                        SizedBox(
                            width: double.infinity,
                            child: Text(
                                dataTransaction[i].service.nameService +
                                    ' (' +
                                    dataTransaction[i].qty.toString() +
                                    ')',
                                style: TextStyle(
                                    fontSize: 12.0.sp,
                                    color: Theme.of(context).primaryColor,
                                    fontWeight: FontWeight.bold))),
                        SizedBox(
                            width: double.infinity,
                            child: Text(
                                'Subtotal ' +
                                    FunctionHelper.moneyChanger(double.parse(
                                        (dataTransaction[i].qty *
                                                dataTransaction[i]
                                                    .service
                                                    .price)
                                            .toString())),
                                style: TextStyle(
                                    fontSize: 11.0.sp, color: Colors.black87))),
                      ],
                    )),
              )
            ],
          ));
        }

        return Container(
          margin: EdgeInsets.only(top: 5.0.w),
          child: Container(
              margin: EdgeInsets.symmetric(horizontal: 5.0.w),
              width: 90.0.w,
              padding: EdgeInsets.all(5.0.w),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.black.withOpacity(0.07)),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(transaction.name,
                          style: TextStyle(
                              fontSize: 12.0.sp,
                              color: Colors.black87,
                              fontWeight: FontWeight.bold)),
                      Text(transaction.transactionDate,
                          style: TextStyle(
                              fontSize: 12.0.sp,
                              color: Theme.of(context).primaryColor))
                    ],
                  ),
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 3.0.w),
                      width: double.infinity,
                      height: 0.3.w,
                      color: Colors.black12),
                  Column(
                    children: dataListLaundry,
                  ),
                  SizedBox(height: 3.0.w),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Total Harga',
                          style: TextStyle(
                              fontSize: 12.0.sp, color: Colors.black54)),
                      Text(
                          FunctionHelper.moneyChanger(
                              double.parse(transaction.totalBill!)),
                          style: TextStyle(
                              fontSize: 12.0.sp, color: Colors.black87))
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Tanggal Selesai',
                          style: TextStyle(
                              fontSize: 12.0.sp, color: Colors.black54)),
                      Text(transaction.finishDate,
                          style: TextStyle(
                              fontSize: 12.0.sp, color: Colors.black87))
                    ],
                  )
                ],
              )),
        );
      }

      List<QueryDocumentSnapshot<Object?>> dataRaw = data.docs;

      List<Widget> dataWidgetMain = [];

      for (var i = 0; i < dataRaw.length; i++) {
        TransactionModel transaction = TransactionModel(
            name: dataRaw[i]['cutomer_name'],
            id: dataRaw[i]['id'],
            transactionDate: dataRaw[i]['start_date'],
            finishDate: dataRaw[i]['end_date'],
            note: dataRaw[i]['notes'],
            totalBill: dataRaw[i]['total_bill'].toString());

        dataWidgetMain.add(StreamBuilder<QuerySnapshot>(
            stream: FirebaseFirestore.instance
                .collection("transaction_detail")
                .snapshots(),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (!snapshot.hasData) return new Text("There is no expense");
              return buildDataFinal(transaction, snapshot.data!);
            }));
      }
      return Column(
        children: dataWidgetMain,
      );
    }

    return StreamBuilder<QuerySnapshot>(
        stream:
            FirebaseFirestore.instance.collection("transaction").snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) return new Text("There is no expense");
          return SingleChildScrollView(child: _buildData(snapshot.data!));
        });
  }
}

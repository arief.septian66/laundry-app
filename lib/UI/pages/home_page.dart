part of 'pages.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: _buildAppBar(),
      body: _buildContent(),
    ));
  }

  Widget _buildContent() {
    Widget _buildBanner() {
      return Container(
          margin: EdgeInsets.symmetric(vertical: 10.0.w),
          width: 60.0.w,
          height: 40.0.w,
          child: Image.asset('assets/home_banner.png'));
    }

    Widget _buildButton() {
      Widget buildButtonDataWidget(
          {required IconData icon,
          required Function() onTap,
          required String title,
          required String description}) {
        return GestureDetector(
          onTap: onTap,
          child: Card(
            child: Container(
                width: 90.0.w,
                padding:
                    EdgeInsets.symmetric(vertical: 2.0.h, horizontal: 5.0.w),
                child: Row(
                  children: [
                    Icon(icon, color: Theme.of(context).primaryColor),
                    SizedBox(width: 3.0.w),
                    Expanded(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(title,
                            style: TextStyle(
                                fontSize: 13.0.sp,
                                color: Colors.black87,
                                fontWeight: FontWeight.bold)),
                        Text(description,
                            style: TextStyle(
                              fontSize: 10.0.sp,
                              color: Colors.black54,
                            ))
                      ],
                    ))
                  ],
                )),
          ),
        );
      }

      return Column(
        children: [
          buildButtonDataWidget(
              icon: Icons.addchart_sharp,
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (_) => TransactionMainPage()));
              },
              title: 'Masukkkan Transaksi',
              description:
                  'Kamu bisa melakukan pencatatan transaksi dan menyimpannya'),
          SizedBox(height: 2.0.w),
          buildButtonDataWidget(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (_) => TransactionHistory()));
              },
              icon: Icons.list_rounded,
              title: 'Daftar Transaksi',
              description:
                  'Kamu bisa melihat semua catatan transaksi yang tersimpan')
        ],
      );
    }

    return SingleChildScrollView(
        child: Container(
      width: 100.0.w,
      child: Column(
        children: [_buildBanner(), _buildButton()],
      ),
    ));
  }

  AppBar _buildAppBar() {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 4,
      automaticallyImplyLeading: false,
      titleSpacing: 0,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Selamat Datang',
            style: TextStyle(
                fontSize: 13.0.sp,
                color: Colors.black87,
                fontWeight: FontWeight.bold),
          ),
          Container(
              width: 4.0.w,
              height: 4.0.w,
              child: Image.asset('assets/party.png'))
        ],
      ),
    );
  }
}

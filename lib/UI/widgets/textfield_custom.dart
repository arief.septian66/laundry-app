part of 'widgets.dart';

class TextFieldCustom {
  static Widget roundedTextfield(
      {required TextEditingController controller,
      required String label,
      Function(String)? onChanged,
      TextInputType? textInputType,
      bool? noBorder}) {
    return Container(
        margin: EdgeInsets.only(top: 5.0.w),
        width: double.infinity,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: Colors.black.withOpacity(0.07)),
        child: TextField(
          controller: controller,
          keyboardType: textInputType,
          onChanged: onChanged,
          decoration: InputDecoration(
              labelText: label,
              border: noBorder != null ? InputBorder.none : null,
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 4.0.w, vertical: 2.0.w)),
        ));
  }
}

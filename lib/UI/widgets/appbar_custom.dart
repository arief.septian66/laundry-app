part of 'widgets.dart';

class BasicAppBar {
  static AppBar basicAppBar({required String title}) {
    return AppBar(
        backgroundColor: Colors.white,
        elevation: 3,
        titleSpacing: 0,
        toolbarHeight: 8.0.h,
        automaticallyImplyLeading: false,
        title: Stack(
          children: [
            Container(
                width: double.infinity,
                height: 8.0.h,
                alignment: Alignment.center,
                child: Text(
                  title,
                  style: TextStyle(
                      fontSize: 13.0.sp,
                      color: Colors.black87,
                      fontWeight: FontWeight.bold),
                )),
            Container(
              height: 8.0.h,
              width: 8.0.h,
              child: Container(
                margin: EdgeInsets.only(left: 5.0.w),
                child: Icon(Icons.arrow_back, color: Colors.black87),
              ),
            ),
          ],
        ));
  }
}

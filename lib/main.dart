import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:laundry/UI/pages/pages.dart';
import 'package:laundry/cubits/cubits.dart';
import 'package:sizer/sizer.dart';

void main() {
  runApp(MultiBlocProvider(providers: [
    BlocProvider<CartCubit>(
      create: (context) => CartCubit(),
    )
  ], child: MyApp()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  Future<FirebaseApp> _initializeFirebase() async {
    FirebaseApp firebaseApp = await Firebase.initializeApp();

    return firebaseApp;
  }

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return FutureBuilder(
        future: _initializeFirebase(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Error initializing Firebase');
          } else if (snapshot.connectionState == ConnectionState.done) {
            return MaterialApp(
              title: 'Flutter Demo',
              debugShowCheckedModeBanner: false,
              theme: ThemeData(
                primaryColor: Color(0xFF4889fa),
                primarySwatch: Colors.blue,
              ),
              home: HomePage(),
            );
          }
          return CircularProgressIndicator();
        },
      );
    });
  }
}

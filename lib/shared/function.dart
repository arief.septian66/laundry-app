part of 'shared.dart';

class FunctionHelper {
  static String milisecondToDays(int data) {
    int daysDivider = 60 * 60 * 24 * 1000;
    int hourDivider = 60 * 60 * 1000;
    int daysCount = data ~/ daysDivider;
    int hourCount = (data - (daysDivider * daysCount)) ~/ hourDivider;

    return daysCount.toString() + ' Hari ' + hourCount.toString() + ' Jam';
  }

  static String dateToReadable(DateTime date) {
    String dateRaw = date.toString().split(' ')[0];
    String year = dateRaw.substring(0, 4);
    String dateFinal = dateRaw.substring(8, 10);

    int monthRaw = int.parse(dateRaw.substring(5, 7));
    String monthFinal = '';

    switch (monthRaw) {
      case 1:
        monthFinal = 'Januari';
        break;
      case 2:
        monthFinal = 'Februari';
        break;
      case 3:
        monthFinal = 'Maret';
        break;
      case 4:
        monthFinal = 'April';
        break;
      case 5:
        monthFinal = 'Mei';
        break;
      case 6:
        monthFinal = 'Juni';
        break;
      case 7:
        monthFinal = 'Juli';
        break;
      case 8:
        monthFinal = 'Agustus';
        break;
      case 9:
        monthFinal = 'September';
        break;
      case 10:
        monthFinal = 'Oktober';
        break;
      case 11:
        monthFinal = 'November';
        break;
      case 12:
        monthFinal = 'Desember';
        break;
      default:
    }

    return '$dateFinal $monthFinal $year ';
  }

  static String moneyChanger(double value) {
    return NumberFormat.currency(name: 'Rp', decimalDigits: 0, locale: 'id')
        .format(value.round());
  }

  static String tokenGenerator({String? q, int? limit, required int offset}) {
    final key = 'adaidelangsungjalan';
    final claimSet = JwtClaim(
      otherClaims: <String, dynamic>{
        'limit': limit ?? 10,
        'q': q ?? '',
        'offset': offset
      },
    );

    String token = issueJwtHS256(claimSet, key);
    return token;
  }
}

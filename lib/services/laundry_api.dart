part of 'services.dart';

class LaundryAPI {
  static Future<ApiReturnValue<List<LaundryService>>> fetchDataService(
      String token) async {
    ApiReturnValue<List<LaundryService>> returnValue;

    final String url = 'https://android-test-fljnd6wana-as.a.run.app/layanan';

    var request = http.MultipartRequest('POST', Uri.parse(url));

    request.fields['jwt'] = token;

    try {
      final streamSend = await request.send();
      final response = await http.Response.fromStream(streamSend);
      var data = json.decode(response.body);

      if (response.statusCode != 200) {
        returnValue =
            ApiReturnValue(data: data, status: RequestStatus.failed_request);
      } else {
        List<LaundryService> dataReturn = [];
        for (var i = 0; i < data.length; i++) {
          dataReturn.add(LaundryService.fromJson(data[i]));
        }
        returnValue = ApiReturnValue(
            status: RequestStatus.success_request, data: dataReturn);
      }
    } on SocketException {
      returnValue =
          ApiReturnValue(data: null, status: RequestStatus.internet_issue);
    } catch (e) {
      print(e);
      returnValue =
          ApiReturnValue(status: RequestStatus.server_error, data: null);
    }

    return returnValue;
  }
}

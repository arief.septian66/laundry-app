part of '../cubits.dart';

abstract class ServiceState extends Equatable {
  const ServiceState();

  @override
  List<Object> get props => [];
}

class ServiceInitial extends ServiceState {}

class ServiceLoading extends ServiceState {}

class ServiceFailed extends ServiceState {}

class ServiceLoaded extends ServiceState {
  final List<LaundryService> data;

  const ServiceLoaded(this.data);

  @override
  List<Object> get props => [data];
}

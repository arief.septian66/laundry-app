part of '../cubits.dart';

class ServiceCubit extends Cubit<ServiceState> {
  ServiceCubit() : super(ServiceInitial());

  void loadData({int? limit, String? q, required int offset}) async {
    if (offset == 0) {
      emit(ServiceLoading());
    }

    String token =
        FunctionHelper.tokenGenerator(limit: limit, offset: offset, q: q);

    LaundryAPI.fetchDataService(token).then((value) {
      if (value.status == RequestStatus.success_request) {
        if (offset == 0) {
          emit(ServiceLoaded(value.data!));
        } else {
          ServiceState serviceState = state;
          if (serviceState is ServiceLoaded) {
            List<LaundryService> dataExist = serviceState.data;
            for (var i = 0; i < value.data!.length; i++) {
              dataExist.add(value.data![i]);
            }
            emit(ServiceLoaded(dataExist));
          }
        }
      } else {
        emit(ServiceFailed());
      }
    });
  }
}

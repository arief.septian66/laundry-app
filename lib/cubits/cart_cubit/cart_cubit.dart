part of '../cubits.dart';

class CartCubit extends Cubit<CartState> {
  CartCubit() : super(CartInitial());

  void addToCart(LaundryService service, int qty) {
    CartState cartState = state;

    if (cartState is CartInitial) {
      emit(CartValue([Cart(qty: qty, service: service)]));
    } else if (cartState is CartValue) {
      List<Cart> cartData = cartState.data;
      bool addNewService = true;
      int updateIndex = 0;
      for (var i = 0; i < cartData.length; i++) {
        if (service.idService == cartState.data[i].service.idService) {
          addNewService = false;
          updateIndex = i;
        }
      }
      if (addNewService) {
        cartData.add(Cart(qty: qty, service: service));
      } else {
        cartData[updateIndex].qty = cartData[updateIndex].qty + qty;
      }
      emit(CartValue(cartData));
    }
  }

  void checkout(BuildContext context, TransactionModel transaction) async {
    final FirebaseFirestore _firestore = FirebaseFirestore.instance;
    final CollectionReference _transactionCollection =
        _firestore.collection('transaction');

    final CollectionReference _transactionDetailCollection =
        _firestore.collection('transaction_detail');

    DocumentReference transactionRef = _transactionCollection.doc();
    DocumentReference detailTransactionRef = _transactionDetailCollection.doc();

    String myId = transactionRef.id;

    Map<String, dynamic> data = <String, dynamic>{
      "id": myId,
      "cutomer_name": transaction.name,
      "total_bill": int.parse(transaction.totalBill!),
      "notes": transaction.note,
      "start_date": transaction.transactionDate,
      "end_date": transaction.finishDate
    };

    await transactionRef
        .set(data)
        .whenComplete(() => print("Berhasil"))
        .catchError((e) => print(e));

    CartState cartState = BlocProvider.of<CartCubit>(context).state;
    if (cartState is CartValue) {
      for (var i = 0; i < cartState.data.length; i++) {
        String id = detailTransactionRef.id;
        Map<String, dynamic> data = <String, dynamic>{
          "id": id,
          "service_id": cartState.data[i].service.idService,
          "transaction_id": myId,
          "service_name": cartState.data[i].service.nameService,
          "qty": cartState.data[i].qty,
          "service_price": cartState.data[i].service.price,
          "total_price": cartState.data[i].service.price * cartState.data[i].qty
        };
        await detailTransactionRef
            .set(data)
            .whenComplete(() => print("Berhasil"))
            .catchError((e) => print(e));
      }
    }

    emit(CartInitial());
    Fluttertoast.showToast(msg: 'Berhasil Simpan Data');
    Navigator.pop(context);
    Navigator.pop(context);
  }
}

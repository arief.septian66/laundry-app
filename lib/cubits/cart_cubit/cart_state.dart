part of '../cubits.dart';

abstract class CartState extends Equatable {
  const CartState();

  @override
  List<Object> get props => [];
}

class CartInitial extends CartState {}

class CartValue extends CartState {
  final List<Cart> data;

  const CartValue(this.data);

  @override
  List<Object> get props => [data];
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:laundry/models/models.dart';
import 'package:laundry/services/services.dart';
import 'package:laundry/shared/shared.dart';

part 'service_cubit/service_cubit.dart';
part 'service_cubit/service_state.dart';
part 'cart_cubit/cart_cubit.dart';
part 'cart_cubit/cart_state.dart';
